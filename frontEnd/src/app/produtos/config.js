import ListController from './list.controller'
import FormController from './form.controller'

export const produtoConfig = ($stateProvider, $urlRouterProvider) => {
  $stateProvider
    .state('produto', {
      template: require('@views/default.html'),
      url: '/produtos',
      onEnter: ['$state', function($state) {
        $state.go('produto.list')
      }]
    })
    .state('produto.list', {
      template: require('@views/produtos/list.html'),
      url: '/produtos',
      controller: ListController,
      controllerAs: 'vm'
    })
    .state('produto.new', {
      template: require('@views/produtos/form.html'),
      url: '/produtos/novo',
      controller: FormController,
      controllerAs: 'vm'
    })
    .state('produto.edit', {
      template: require('@views/produtos/form.html'),
      url: '/produtos/{id}',
      controller: FormController,
      controllerAs: 'vm'
    });
};

produtoConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
