export default class ListController {

    constructor() {
        this.records = [{
            id: 1,
            documento: '1',
            nome: 'João',
            email: 'joao@gmail.com',
            telefone: '254252452'
        },{
            id: 2,
            documento: '11',
            nome: 'Pedro',
            email: 'pedro@ig.com',
            telefone: '234523'
        },{
            id: 3,
            documento: '111',
            nome: 'Antônio',
            email: 'antonio@yoahoo.com.br',
            telefone: '235235'
        },{
            id: 4,
            documento: '11111',
            nome: 'Desidério',
            email: 'desiderio@uol.com',
            telefone: '245235'
        }];
    }
}
