import ListController from './list.controller'
import FormController from './form.controller'

export const mesaConfig = ($stateProvider, $urlRouterProvider) => {
  $stateProvider
    .state('mesa', {
      template: require('@views/default.html'),
      url: '/mesas',
      onEnter: ['$state', function($state) {
        $state.go('mesa.list')
      }]
    })
    .state('mesa.list', {
      template: require('@views/mesas/list.html'),
      url: '/mesas',
      controller: ListController,
      controllerAs: 'vm'
    })
    .state('mesa.new', {
      template: require('@views/mesas/form.html'),
      url: '/mesas/novo',
      controller: FormController,
      controllerAs: 'vm'
    })
    .state('mesa.edit', {
      template: require('@views/mesas/form.html'),
      url: '/mesas/{id}',
      controller: FormController,
      controllerAs: 'vm'
    });
};

mesaConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
