(function () {
    'use strict'

    angular.module('app')
        .service('MesaService', MesaService);

    MesaService.$inject = ['$http'];

    function MesaService($http) {
        var idMesa;

        function findPedidoEmAberto(idMesa) {
            return $http.get('https://comanda-free.herokuapp.com/api/mesas/pedido-em-aberto?mesa=' + idMesa)
                .then(function (response) {
                    return response.data.id;
                });
        }

        function findById(id) {
            return $http.get('https://comanda-free.herokuapp.com/api/mesas/' + id)
                .then(function (response) {
                    return response.data;
                });
        }
        function findAllOver() {
            return $http.get('https://comanda-free.herokuapp.com/api/mesas/all?order=numero')
                .then(function (response) {
                    return response.data;
                });
        }
        function findAll() {
            return $http.get('https://comanda-free.herokuapp.com/api/mesas')
                .then(function (response) {
                    return {registros: response.data
                        }   
                });
        }

        function insert(registro) {
            return $http.post('https://comanda-free.herokuapp.com/api/mesas', registro)
                .then(function (response) {
                    return response.data;
                });
        }

        function update(registro) {
            return $http.put('https://comanda-free.herokuapp.com/api/mesas/' + registro.id, registro)
                .then(function (response) {
                    return response.data;
                });
        }

        function remove(id) {
            return $http.delete('https://comanda-free.herokuapp.com/api/mesas/' + id)
                .then(function (response) {
                    return response.data;
                });
        }

        return {
            idMesa:idMesa,
            findPedidoEmAberto:findPedidoEmAberto,
            findAllOver: findAllOver,
            findById:findById,
            findAll: findAll,
            insert: insert,
            update: update,
            remove: remove
        }
    }

})();