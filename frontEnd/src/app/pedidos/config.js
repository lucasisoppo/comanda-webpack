import ListController from './list.controller'
import FormController from './form.controller'

export const pedidoConfig = ($stateProvider, $urlRouterProvider) => {
  $stateProvider
    .state('pedido', {
      template: require('@views/default.html'),
      url: '/pedidos',
      onEnter: ['$state', function($state) {
        $state.go('pedido.list')
      }]
    })
    .state('pedido.list', {
      template: require('@views/pedidos/list.html'),
      url: '/pedidos',
      controller: ListController,
      controllerAs: 'vm'
    })
    .state('pedido.new', {
      template: require('@views/pedidos/form.html'),
      url: '/pedidos/novo',
      controller: FormController,
      controllerAs: 'vm'
    })
    .state('pedido.edit', {
      template: require('@views/pedidos/form.html'),
      url: '/pedidos/{id}',
      controller: FormController,
      controllerAs: 'vm'
    });
};

pedidoConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
