export default function inputDirective() {
    return{
        restrict: 'E',
        replace: true,
        template: require('./input.html'),
        scope: {
            label: '@',
            type: '@',
            validacao: '@'
        }
    }
}