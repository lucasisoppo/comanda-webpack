import angular from 'angular';
import menuDirective from './menu/menu.directive';
import inputDirective from './input/input.directive';

export default angular.module('diretivas', [])
    .directive('menu', menuDirective)
    .directive('appInput', inputDirective)
    .name;