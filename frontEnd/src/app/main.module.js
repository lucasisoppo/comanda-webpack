import angular from 'angular';

import { default as uiRouter } from '@uirouter/angularjs';
import diretivas from './diretivas';

import { mainConfig } from './main/config';
import { clienteConfig } from './clientes/config';
// import { produtoConfig } from './produtos/config';
///import { pedidoConfig } from './pedidos/config';
//import { mesaConfig } from './mesas/config';

export default angular.module('app', [uiRouter, diretivas])
  .config(mainConfig)
  .config(clienteConfig)
  // .config(produtoConfig)
 // .config(pedidoConfig)
  //.config(mesaConfig)
  .name;
