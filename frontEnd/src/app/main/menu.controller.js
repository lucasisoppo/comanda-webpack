export default class MenuController {
    constructor() {
        this.itens = [
            {
                state:'app.dashboard',
                icon: 'icon-home',
                name: 'Dashboard'
            },{
                state:'app.cliente',
                icon: 'icon-users',
                name: 'Clientes'
            },{
                state:'app.itens',
                icon: 'icon-shopping-cart',
                name: 'Itens'
            },{
                state:'app.pedido',
                icon: 'icon-file',
                name: 'Pedidos'
            }
        ]
    }
}