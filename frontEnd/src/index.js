import mainModule from './app/main.module';

import './style.scss';
require('bootstrap');

angular.bootstrap(document.body, [mainModule], { strictDi: true });
